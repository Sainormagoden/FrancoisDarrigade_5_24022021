/**
 * API for cameras
 * online API : 'https://oc5-orinoco-backend-api.herokuapp.com/api/cameras/'
 * local API (if back-end is install and start) : 'http://localhost:3000/api/cameras/'
 */
const api = 'https://oc5-orinoco-backend-api.herokuapp.com/api/cameras/'; 

/**
 * Class for create new contact with form data.
 * @constructor @param {string} firstName The user firstname.
 * @constructor @param {string} lastName The user lastName.
 * @constructor @param {string} address The user address.
 * @constructor @param {string} city The user city.
 * @constructor @param {string} email The user email.
 */
class Contact {
    constructor (firstName, lastName, address, city, email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.email = email;
    }
};

/**
 * Gets one or many products from the store API.
 * If the product id isn't provided we'll get the full products list.
 * @param {string} id The product id (optinal).
 * @returns {Promise<object || [] object>} Returns a promise that represents one or many products.
 */
export const getProducts = (id='') => {
    
    // Create a Promise.
    return new Promise ((resolve, reject) => {

        // Create and open a request on GET.
        var request = new XMLHttpRequest();
        request.open('GET', api+id);
        request.onreadystatechange = function() {

            // If request is done and his status is 200, return response.
            if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                var response = JSON.parse(this.responseText);
                resolve(response);
            } else if (this.readyState == XMLHttpRequest.DONE && this.status != 200) {
                reject(this.status);
            };
        };
        request.send();
    });
};

/**
 * Send order at API.
 * @param {object} order Object with contact class and the list of id of product. It's create with function 'SetOrder'.
 * @returns {Promise<object>} Return a API response.
 */
 const postOrder = (order) => {
    
    // Create a Promise.
    return new Promise((resolve, reject) => {

        // Create and open a request on GET.
        var request = new XMLHttpRequest();
        request.open('POST', api+'order');
        request.setRequestHeader('Content-Type', 'application/json');
        request.onreadystatechange = function() {
            if (this.readyState == XMLHttpRequest.DONE && this.status == 201) {

                // If request is done and his status is 201, return response.
                var response = JSON.parse(this.responseText);
                resolve(response);
            } else if (this.readyState == XMLHttpRequest.DONE && this.status != 201) {
                reject(this.status);
            };
        };
        request.send(order);
    });
};

/**
 * Set object for postOrder and return promise.
 * @param {string} firstName The user firstname.
 * @param {string} lastName The user lastName.
 * @param {string} address The user address.
 * @param {string} city The user city.
 * @param {string} email The user email.
 * @param {object} listId The user list of id product.
 * @returns {Promise<object>} return a API response.
 */
export const SetOrder = (firstName, lastName, address, city, email, listId) =>{
    let myContact = new Contact(firstName, lastName, address, city, email);
    let order = JSON.stringify({
        'contact':myContact,
        'products':listId
    })
    let response = postOrder(order);
    return response;
};