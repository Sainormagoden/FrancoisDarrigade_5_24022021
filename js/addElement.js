// Import JS.
import * as request from './request';
import * as storage from './localStorage';

/**
 * Show products on different page.
 * @returns {void}
 */
export const showProducts = () => {

    // Get data according to the current page. 
    let myPage = dataPage();

    // Check if myPage have a value.
    if (myPage.currentPage != '') {
        let content;
        let targetContainer;
        let btn;
        switch(myPage.currentPage) {
            case 'index' : {
                content = document.querySelector('template').content;
                targetContainer = document.querySelector(myPage.target);
                request.getProducts() 
                .then((result) => {
                    
                    // Display none all elements with class 'loading'.
                    hideLoading();
                    result.forEach(
                        (element) => {
                            if (myPage.search != '') {
                                document.getElementById('search__text').textContent = myPage.search;
                                document.getElementById('search-form__text').value = myPage.search;
                            };
                            if (myPage.search == '' || element.name.toLowerCase().includes(myPage.search.toLowerCase()) || element.description.toLowerCase().includes(myPage.search.toLowerCase())) {
                                showIndexCards(element, content, targetContainer);
                            };
                        }
                    );

                    // Show fail text.
                    if (document.getElementsByClassName('products__card').length === 0){
                        document.getElementById('search__fail').classList.remove('d-none');
                    };

                    // Add event for sort.
                    document.getElementById('tri_select').addEventListener('change', (event) => {
                        sortBy(event.target.value);
                    });

                    // Change filters.
                    showFilters(document.getElementById('filter_form')); 
                    document.getElementById('filter_form').addEventListener('change', () => {
                        filterBy();
                    });
                    document.querySelector('#filter_form').addEventListener('reset', () => {
                        filterBy();
                    });
                })
                .catch((error) => {
                    alertError('Erreur lors de la récupération des produits.');
                });
                break;
            }
            case 'product' : {
                content = document.querySelector('template').content;
                targetContainer = document.querySelector(myPage.target);
                request.getProducts(myPage.id) 
                .then((result) => {

                    // Display none all elements with class 'loading'.
                    hideLoading();
                    showProduitDetail(result, content, targetContainer);
                    btn = document.getElementById('product_detail__add');
                    btn.addEventListener('click', () => {

                        // If 'addBasket' return false.
                        if (!storage.addBasket(result._id)) {
                            alertError('Echec lors de l\'ajout au panier.');
                        } else {
                            btn.setAttribute('data-toggle', 'modal');
                            btn.setAttribute('data-target', '#validModal');
                        };
                    });
                })
                .catch((error) => {
                    alertError('Aucun ou mauvais id utilisé.');
                });
                break;
            }
            case 'basket' : {
                let myStorage = storage.getListId();
                content = document.querySelector('template').content;
                targetContainer = document.querySelector(myPage.target);
                btn = document.getElementById('basket__clear');
                btn.addEventListener('click', (event) => {
                    if (!storage.clearBasket(event.target.getAttribute('data-id'))) {
                        alertError('Echec lors de la suppression du panier.');
                    };                    
                });
                totalPrice('#basket__list__priceSum span');

                // Show all product in order.
                if (myStorage.length !== 0) {  
                    let i = 0;
                    myStorage.forEach(
                        (id) => {
                            request.getProducts(id)
                            .then((element) =>{
                                i++;
                                showOrderList(element, content, targetContainer);

                                // Add event for delete one product.
                                if (i === myStorage.length) {
                                    Array.from(document.getElementsByClassName('delete_product')).forEach((element) => {
                                        element.addEventListener('click', (event) => {
                                            btn.setAttribute('data-id', event.target.closest('tr').getAttribute('id'));
                                        });
                                    });
                                    document.querySelector('#remove_all').addEventListener('click', (event) => {
                                        btn.setAttribute('data-id', '');
                                    });
                                };                               
                            })
                            .catch((error) => {
                                alertError('Erreur lors de la récupération des produits.');
                            });               
                        }
                    );
                } else {
                    document.querySelector('table').classList.add('d-none');
                    document.querySelector('#remove_all').classList.add('d-none');
                    document.querySelector('#basket__list__empty').classList.remove('d-none');
                    disabledForm(document.getElementById("basket__form"));
                };

                // Add event on form when submit.
                document.getElementById("basket__form").onsubmit = () => {
                    if (window.sessionStorage.getItem('orinocoPrice') === document.querySelector('#basket__list__priceSum span').textContent) {
                        postForm(document.getElementById("basket__form"), myStorage);
                    } else {
                        alertError('Le prix à payer ne correspond pas à celui afficher.');
                    };
                    return false;
                };
                break;
            }
            case 'confirm' : {
                showConfirmResult('#confirm__price', '#confirm__id');
                break;
            }
            case 'error' : {
                showErrorResult('#error__alert');
                break;
            }
            default : {
                alertError('Page inconnue');
                break;
            }
        };
    }
    else{
        alertError('Page non reconnue');
    };
};

/**
 * Show product card.
 * Display none all contents with 'loading' class.
 * Clone template and change : 
 *  - image src and alt.
 *  - title, description, price.
 *  - button link.
 * @param {object} element Product information.
 * @param {object} content Template for clone it.
 * @param {object} targetContainer Indicates where create new content.
 * @returns {void}
 */
const showIndexCards = (element, content, targetContainer) =>{

    // Clone content and apply modifications.
    let newContent = content;
    newContent.querySelector('source').setAttribute('srcset', verifUrl(element.imageUrl));
    newContent.querySelector('img').setAttribute('alt', 'Image de la camera '+element.name);
    newContent.querySelector('.card-title').textContent  = element.name;
    newContent.querySelector('.card-text__description').textContent  = element.description;
    newContent.querySelector('.card-text span').textContent  = priceFormat(element.price);
    newContent.querySelector('.card-link').setAttribute('href', './produit.html?id='+element._id);
    newContent.querySelector('.card-link').textContent = 'Voir les détails';
    newContent.querySelector('.card').setAttribute('data-lenses', element.lenses);

    // Create new element.
    targetContainer.appendChild(document.importNode(newContent, true));
};

/**
 * Show product detail.
 * Display none content with 'loading' class.
 * Clone template and change : 
 *  - image src and alt.
 *  - background image.
 *  - title, description, price.
 *  - add options for personnalisation.
 * @param {object} element Product information.
 * @param {object} content Template for clone it.
 * @param {object} targetContainer Indicates where create new content.
 * @returns {void}
 */
const showProduitDetail = (element, content, targetContainer) => {
    
    // Clone content and apply modifications
    let newContent = content;
    newContent.querySelector('#product_detail__header source').setAttribute('srcset', verifUrl(element.imageUrl));
    newContent.querySelector('#product_detail__header img').setAttribute('alt', 'Image de la camera '+element.name);
    newContent.querySelector('#product_detail__header__blur').style.backgroundImage = 'url('+verifUrl(element.imageUrl)+')';
    newContent.querySelector('#product_detail__content h1').textContent  = element.name;
    newContent.querySelector('#product_detail__content p').textContent  = element.description;
    newContent.querySelector('#product_detail__content h2 span').textContent  = priceFormat(element.price);
    element.lenses.forEach((e) =>{
        let option = document.createElement('option');
        option.textContent = e;
        option.setAttribute('value', e)
        newContent.querySelector('#product_detail__content #custom_select').add(option);
    });

    // Create new element
    targetContainer.appendChild(document.importNode(newContent, true));
};

/**
 * Show order list.
 * Clone template and change : 
 *  - image src and alt.
 *  - background image.
 *  - title, description, price.
 * @param {object} element Product information.
 * @param {object} content Template for clone it.
 * @param {object} targetContainer Indicates where create new content.
 * @returns {void}
 */
const showOrderList = (element, content, targetContainer) => {

    // If product is already show, add number and price.
    if (document.getElementById(element._id)) {
        let price = priceFormat(element.price);
        document.getElementById(element._id).querySelector('.table__number').textContent = parseFloat(
            document.getElementById(element._id).querySelector('.table__number').textContent) + 1;
        document.getElementById(element._id).querySelector('.table__price span').textContent  =  (parseFloat(
            document.getElementById(element._id).querySelector('.table__price span').textContent) + parseFloat(price)).toFixed(2);
    } else {

        // Clone content and apply modifications.
        let newContent = content;
        newContent.querySelector('tr').setAttribute('id', element._id);
        newContent.querySelector('.table__source').setAttribute('srcset', verifUrl(element.imageUrl)); 
        newContent.querySelector('.table__img').setAttribute('alt', 'Image de la camera '+element.name);
        newContent.querySelector('.table__link').setAttribute('href', './produit.html?id='+element._id);
        newContent.querySelector('.table__name').textContent  = element.name; 
        newContent.querySelector('.table__price span').textContent  =  priceFormat(element.price);
    
        // Create new element.
        targetContainer.appendChild(document.importNode(newContent, true));
    }
};

/**
 * Modify elements in confirm.html.
 * @param {string} targetPrice Indicates where modify the price.
 * @param {string} targetId Indicates where modify the id order.
 * @returns {void}
 */
const showConfirmResult = (targetPrice, targetId) => {
    if(window.sessionStorage.getItem('orinocoPrice') && window.sessionStorage.getItem('orinocoOrderId')){
        document.querySelector(targetPrice).textContent  = window.sessionStorage.getItem('orinocoPrice');
        document.querySelector(targetId).textContent  = window.sessionStorage.getItem('orinocoOrderId');
    } else {
        window.location.href = './404.html';
    };
};

/**
 * Modify elements in 404.html.
 * @param {string} targetError Indicates where modify the error.
 * @returns {void}
 */
 const showErrorResult = (targetError) => {
    if(window.sessionStorage.getItem('orinocoError')){
        document.querySelector(targetError).textContent  = window.sessionStorage.getItem('orinocoError');
    }
};

/**
 * Change the format at price for show it.
 * Before : 49000.
 * After : 499.00.
 * @param {number} price Product price in number.
 * @returns {string} Product price in string.
 */
const priceFormat = (price) => {
    return price.toString().substr(0, price.toString().length-2) + '.' + price.toString().substr(price.toString().length-2,);
};

/**
 * Calcul the price total and place it in taget element.
 * Add price in sessionStorage.
 * @param {string} targetChange Indicates where modify the price.
 * @returns {string} return total price.
 */
const totalPrice = (targetChange) => {
    let price = 0;
    let myStorage = storage.getListId();
    if (myStorage.length !== 0){  
        myStorage.forEach(
            (id) =>{
                request.getProducts(id)
                .then((element) =>{
                    price += element.price;  
                    document.querySelector(targetChange).textContent = priceFormat(price);
                    window.sessionStorage.setItem('orinocoPrice', priceFormat(price));
                })
                .catch((error) => {
                    alertError('Erreur lors de la récupération des produits pour le calcul du prix.');
                });              
            }
        );
    };
};

/**
 * Send order and redirect on confirm.html.
 * @param {object} form Form with user datas.
 * @param {object} listId List of product for order.
 * @returns {void}
 */
const postForm = (form, listId) => {
    disabledForm(form);
    let formData = Array.from(form.querySelectorAll('input')).reduce((acc, input) => ({...acc, [input.id]: input.value}), {});
    request.SetOrder(formData.basket__form__firstname, formData.basket__form__lastname, formData.basket__form__address,
        formData.basket__form__city, formData.basket__form__email, listId)
        .then((result)=>{
            window.sessionStorage.setItem('orinocoOrderId', result.orderId);
            storage.clearBasket();
            window.location.href = form.getAttribute('action');
        })
        .catch((error) => {
            alertError('Erreur lors de l\'envoi de la commande.');
        });          
};

/**
 * Disable all inputs and the button.
 * @param {object} form Form to disabled.
 * @returns {void}
 */
const disabledForm = (form) =>{
    form.querySelector("button").textContent += "... Desactivé";
    form.querySelector('button').setAttribute('disabled', true);
    form.querySelectorAll('input').forEach((input)=>input.setAttribute('disabled', true));
};

/**
 * Check and change url on 'https' if it's in 'http'.
 * @param {string} url Product url for image.
 * @returns {string} Correct url.
 */
 const verifUrl =  (url) => {
    if (url.indexOf('http://') == 0){
        url = url.replace('http://', 'https://')
    }
    return url;
};

/**
 * Assign different data according to the current page.
 * @returns {object} Return 3 datas : currentPage, target and id.
 */
const dataPage = () => {
    const urlParams = new URLSearchParams(window.location.search);
    let currentPage;
    let target;
    let id;
    let search;

    // If "I'm in produit.html".
    if (window.location.pathname.includes('/produit.html')) {
        currentPage = 'product';
        target = '#product_detail';

        // If there is a id in url
        if (urlParams.get('id')) {
            id = urlParams.get('id');
        } else {
            id = 'error';
        };
    } else if (window.location.pathname.includes('/basket.html')) { // if "I'm in basket.html".
        currentPage = 'basket';
        target = 'tbody';
    } else if (window.location.pathname.includes('/confirm.html')) { // if "I'm in confirm.html".
        currentPage = 'confirm';
    } else if (window.location.pathname.includes('/404.html')) { // if "I'm in confirm.html".
    currentPage = 'error';
    } else { // if "I'm in index.html".
        currentPage = 'index';
        target = '#products';
        if (urlParams.get('search')) {
            search = urlParams.get('search');
        } else {
            search = '';
        };
    };
    return  { currentPage: currentPage, target: target, id: id, search: search };
};

/**
 * Redirect to 404 page and record a message for indicate there is a problem.
 * @param {string} msgError Indicate the reason for the problem.
 * @returns {void}
 */
const alertError = (msgError) => {
    window.sessionStorage.setItem('orinocoError', msgError);
    window.location.href = './404.html';
};

/**
 * Hide elements with class loading
 * @returns {void}
 */
const hideLoading = () => {
    for (let item of document.getElementsByClassName('loading')) {
        item.style.display = "none";
    };
};

/**
 * Sort product cards.
 * @param {string} optionValue Option the user has chosen.
 * @returns {void}
 */
const sortBy = (optionValue) => {
    
    // Differents sort_by function.
    const sort_by_name_asc = (a, b) => {
        return a.querySelector('.card-title').innerHTML.toLowerCase().localeCompare(b.querySelector('.card-title').innerHTML.toLowerCase());
    }
    const sort_by_name_desc = (a, b) => {
        return b.querySelector('.card-title').innerHTML.toLowerCase().localeCompare(a.querySelector('.card-title').innerHTML.toLowerCase());
    }
    const sort_by_price_asc = (a, b) => {
        return (parseFloat(a.querySelector('.card-text span').innerHTML) - parseFloat(b.querySelector('.card-text span').innerHTML));
    }
    const sort_by_price_desc = (a, b) => {
        return parseFloat(b.querySelector('.card-text span').innerHTML) - parseFloat(a.querySelector('.card-text span').innerHTML);
    }

    // Apply the sort.
    let list = document.getElementsByClassName('products__card');
    list = Array.from(list);    
    switch(optionValue) {   
        case 'alphaDown' : {    
            list.sort(sort_by_name_asc);
            break;
        }
        case 'alphaDownAlt' : {
            list.sort(sort_by_name_desc);
            break;
        }
        case 'numDown' : {
            list.sort(sort_by_price_asc);
            break;
        }
        case 'numDownAlt' : {
            list.sort(sort_by_price_desc);
            break;
        }
        default : {
            list.sort(sort_by_name_asc);
            break;
        }
    }

    // Show cards after sort.
    for (var i = 0; i < list.length; i++) {
        list[i].parentNode.appendChild(list[i]);
    }
};

/**
 * Get and show all products lenses for filters.
 * @param {object} template Template of a checkbox.
 * @return {void}
 */
const showFilters = (form) => {
    let products = document.getElementsByClassName('products__card');
    let filters = [];
    Array.from(products).forEach((lenses) => {
        lenses = lenses.querySelector('.card').getAttribute('data-lenses').split(',');
        lenses.forEach((lense) => {
            if (Object.values(filters).indexOf(lense) === -1) {
                filters.push(lense);
            }
        })
    });
    if (filters.length !== 0) {     
        let div;   
        filters.sort();
        filters.forEach((filter) => { 
            div = form.querySelector('.template--checkbox');    
            div.querySelector('input').setAttribute('id', filter.replace(/\s/g, ''));
            div.querySelector('input').setAttribute('value', filter);
            div.querySelector('label').setAttribute('for', filter.replace(/\s/g, ''));
            div.querySelector('label').textContent = filter;
            div.parentNode.appendChild(document.importNode(div, true));
        });
        div.remove();
        form.classList.remove('d-none');
        form.parentNode.querySelector('p').classList.add('d-none');
    };
};

/**
 * Hide or show elements according to checkbox
 */
const filterBy = () => {

    // Use setTimeout for to act after submit.
    setTimeout(() => {
        let filters = document.querySelectorAll('#filter [type="checkbox"]:checked');
        let products = document.getElementsByClassName('products__card');
        Array.from(products).forEach((product) => {
            product.classList.remove('d-none');
        });
        if (filters.length !== 0) {
            filters.forEach((filter) => {
                Array.from(products).forEach((product) => {
                    if (!product.querySelector('.card').getAttribute('data-lenses').includes(filter.value))
                    product.classList.add('d-none');
                });
            });
        };
        
        // Show fail text.
        if (document.querySelectorAll('.products__card:not(.d-none)').length === 0) {
            document.getElementById('search__fail').classList.remove('d-none');
        } else {
            document.getElementById('search__fail').classList.add('d-none');
        };
    }, 1);
};