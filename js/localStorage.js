// Name at item for localStorage.
const nameItem = 'orinocoId';

/**
 * Add id of product in item 'orinocoId' in localStorage.
 * @param {string} id Id of product.
 * @returns {boolean} Return true if function work else false.
 */
export const addBasket = (id) => {
    let listBasket = getListId();
    if (typeof listBasket === 'object') { 
        listBasket.push(id);
        localStorage.setItem(nameItem, JSON.stringify(listBasket));
        return true;
    } else {
        return false;
    }
};

/**
 * Return list of items in 'orinocoId' in localStorage.
 * If 'orinocoId' not exist, return void array.
 * @returns {object} Return list of items.
 */
export const getListId = () => {
    let listBasket;
    if (localStorage.getItem(nameItem) !== null) {
        listBasket = JSON.parse(localStorage.getItem(nameItem));
    } else {
        listBasket=[];
    }
    return listBasket;
};

/**
 * Clear one or all value in 'orinocoId' in localStorage and reload page.
 * @param {string} id Id of product for delete it (optional).
 * @returns {boolean} Return true if function work.
 */
export const clearBasket = (id = '') => {
    if (id !== '') {
        let listBasket = getListId();
        if (typeof listBasket === 'object') { 
            let clearBasket = [];
            Array.from(listBasket).forEach(element => {
                if (element !== id) {
                    clearBasket.push(element);
                };
            });
            localStorage.setItem(nameItem, JSON.stringify(clearBasket));
        };
    } else {
        localStorage.removeItem(nameItem);
    };
    document.location.reload();
    return true;
}